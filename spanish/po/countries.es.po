# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004-2011
# David Martínez Moreno <ender@debian.org> 2003,2007,2010
#
msgid ""
msgstr ""
"Project-Id-Version: Countries webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-02-15 21:12+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Emiratos Árabes Unidos"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albania"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenia"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Austria"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australia"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosnia-Herzegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesh"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Bélgica"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgaria"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brasil"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamas"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Bielorrusia"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Canadá"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Suiza"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "China"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Colombia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "República Checa"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Alemania"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dinamarca"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "República Dominicana"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Argelia"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ecuador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estonia"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egipto"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "España"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiopía"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finlandia"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Islas Feroe"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Francia"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Reino Unido"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Granada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Groenlandia"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Grecia"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Croacia"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Hungría"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonesia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Irán"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irlanda"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "India"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Islandia"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italia"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordania"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japón"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenia"

#: ../../english/template/debian/countries.wml:267
msgid "Korea"
msgstr "Corea"

#: ../../english/template/debian/countries.wml:270
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:273
msgid "Kazakhstan"
msgstr "Kazakistán"

#: ../../english/template/debian/countries.wml:276
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:279
msgid "Lithuania"
msgstr "Lituania"

#: ../../english/template/debian/countries.wml:282
msgid "Luxembourg"
msgstr "Luxemburgo"

#: ../../english/template/debian/countries.wml:285
msgid "Latvia"
msgstr "Letonia"

#: ../../english/template/debian/countries.wml:288
msgid "Morocco"
msgstr "Marruecos"

#: ../../english/template/debian/countries.wml:291
msgid "Moldova"
msgstr "Moldavia"

#: ../../english/template/debian/countries.wml:294
msgid "Montenegro"
msgstr "Montenegro"

#: ../../english/template/debian/countries.wml:297
msgid "Madagascar"
msgstr "Madagascar"

#: ../../english/template/debian/countries.wml:300
msgid "Macedonia, Republic of"
msgstr "Macedonia, República de"

#: ../../english/template/debian/countries.wml:303
msgid "Mongolia"
msgstr "Mongolia"

#: ../../english/template/debian/countries.wml:306
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:309
msgid "Mexico"
msgstr "México"

#: ../../english/template/debian/countries.wml:312
msgid "Malaysia"
msgstr "Malasia"

#: ../../english/template/debian/countries.wml:315
msgid "New Caledonia"
msgstr "Nueva Caledonia"

#: ../../english/template/debian/countries.wml:318
msgid "Nicaragua"
msgstr "Nicaragua"

#: ../../english/template/debian/countries.wml:321
msgid "Netherlands"
msgstr "Países Bajos"

#: ../../english/template/debian/countries.wml:324
msgid "Norway"
msgstr "Noruega"

#: ../../english/template/debian/countries.wml:327
msgid "New Zealand"
msgstr "Nueva Zelanda"

#: ../../english/template/debian/countries.wml:330
msgid "Panama"
msgstr "Panamá"

#: ../../english/template/debian/countries.wml:333
msgid "Peru"
msgstr "Perú"

#: ../../english/template/debian/countries.wml:336
msgid "French Polynesia"
msgstr "Polinesia francesa"

#: ../../english/template/debian/countries.wml:339
msgid "Philippines"
msgstr "Filipinas"

#: ../../english/template/debian/countries.wml:342
msgid "Pakistan"
msgstr "Pakistán"

#: ../../english/template/debian/countries.wml:345
msgid "Poland"
msgstr "Polonia"

#: ../../english/template/debian/countries.wml:348
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:351
msgid "Réunion"
msgstr "Reunión"

#: ../../english/template/debian/countries.wml:354
msgid "Romania"
msgstr "Rumanía"

#: ../../english/template/debian/countries.wml:357
msgid "Serbia"
msgstr "Serbia"

#: ../../english/template/debian/countries.wml:360
msgid "Russia"
msgstr "Rusia"

#: ../../english/template/debian/countries.wml:363
msgid "Saudi Arabia"
msgstr "Arabia Saudí"

#: ../../english/template/debian/countries.wml:366
msgid "Sweden"
msgstr "Suecia"

#: ../../english/template/debian/countries.wml:369
msgid "Singapore"
msgstr "Singapur"

#: ../../english/template/debian/countries.wml:372
msgid "Slovenia"
msgstr "Eslovenia"

#: ../../english/template/debian/countries.wml:375
msgid "Slovakia"
msgstr "República Eslovaca"

#: ../../english/template/debian/countries.wml:378
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:381
msgid "Thailand"
msgstr "Tailandia"

#: ../../english/template/debian/countries.wml:384
msgid "Tajikistan"
msgstr "Tajikistán"

#: ../../english/template/debian/countries.wml:387
msgid "Tunisia"
msgstr "Túnez"

#: ../../english/template/debian/countries.wml:390
msgid "Turkey"
msgstr "Turquía"

#: ../../english/template/debian/countries.wml:393
msgid "Taiwan"
msgstr "Taiwán"

#: ../../english/template/debian/countries.wml:396
msgid "Ukraine"
msgstr "Ucrania"

#: ../../english/template/debian/countries.wml:399
msgid "United States"
msgstr "Estados Unidos"

#: ../../english/template/debian/countries.wml:402
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:405
msgid "Uzbekistan"
msgstr "Uzbekistán"

#: ../../english/template/debian/countries.wml:408
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:411
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:414
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:417
msgid "South Africa"
msgstr "Sudáfrica"

#: ../../english/template/debian/countries.wml:420
msgid "Zimbabwe"
msgstr "Zimbabue"

#~ msgid "Yugoslavia"
#~ msgstr "Yugoslavia"

#~ msgid "Great Britain"
#~ msgstr "Reino Unido"
