#use wml::debian::translation-check translation="bbdc5fdd9cf31c8e006e51f38f90961f3cb78f42"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en Subversion, un sistema de control
de versiones. El proyecto «Vulnerabilidades y exposiciones comunes» («Common Vulnerabilities and Exposures») identifica los
problemas siguientes:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

    <p>Ace Olszowka informó de que el proceso servidor svnserve de Subversion
    puede terminar su ejecución cuando una petición de solo lectura bien construida produce una respuesta
    determinada, dando lugar a denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

    <p>Tomas Bortoli informó de que el proceso servidor svnserve de Subversion
    puede terminar su ejecución cuando un cliente envía determinadas secuencias de órdenes de protocolo.
    Si la configuración del servidor tiene habilitado el acceso anónimo, esto podría
    dar lugar a denegación de servicio remota sin autenticar.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 1.9.5-1+deb9u4.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1.10.4-1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de subversion.</p>

<p>Para información detallada sobre el estado de seguridad de subversion, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4490.data"
