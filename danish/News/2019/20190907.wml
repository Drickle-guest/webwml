#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7"
<define-tag pagetitle>Opdateret Debian 10: 10.1 udgivet</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den første opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction acme-tiny 				"Håndterer kommende ændringer til ACME-protokollen">
<correction android-sdk-meta 			"Ny opstrømsudgave; retter regex til tilføjelse af Debian-version til binære pakker">
<correction apt-setup 				"Retter preseeding af Secure Apt til lokale arkiv gennem apt-setup/localX/">
<correction asterisk 				"Retter bufferoverløb i res_pjsip_messaging [AST-2019-002 / CVE-2019-12827]; retter sårbarhed i forbindelse med fjernudførelse i chan_sip [AST-2019-003 / CVE-2019-13161]">
<correction babeltrace 				"Opdaterer ctf-symbol-afhængigheder til version efter merge">
<correction backup-manager 			"Retter tømning af fjernarkiver via FTP eller SSH">
<correction base-files 				"Opdaterer til denne punktopdatering">
<correction basez 				"Dekoder på korrekt vis base64url-enkodede strenge">
<correction bro 				"Sikkerhedsrettelser [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 				"Retter regression ved udpakning af nogle filer">
<correction cacti 				"Retter nogle problemer ved opgradering fra versionen i stretch">
<correction calamares-settings-debian 		"Retter rettigheder for initramfs-filaftryk når fuld disk-kryptering er aktiveret [CVE-2019-13179]">
<correction ceph 				"Genopbygger mod ny libbabeltrace">
<correction clamav 				"Forhindrer udpakning af ikke-rekursive zip-bomber; ny stabil opstrømsudgave med sikkerhedsrettelser - tilføjer scanningstidsbegræsning til afhjælpning af zip-bomber [CVE-2019-12625]; retter skrivning udenfor grænserne i NSIS's bzip2-biblioteket [CVE-2019-12900]">
<correction cloudkitty 				"Retter opbygningsfejl med opdateret SQLAlchemy">
<correction console-setup 			"Retter internationaliseringsproblemer ved skift af locales med Perl &gt;= 5.28">
<correction cryptsetup 				"Retter understøttelse af LUKS2-headere uden noget forbundet keyslot; retter mappede segmentoverløb på 32 bit-arkitekturer">
<correction cups 				"Retter adskillige sikkerheds-/blotlæggelsesproblemer - SNMP-bufferoverløb [CVE-2019-8696 CVE-2019-8675], IPP-bufferoverløb, lammelsesangreb og hukommelsesafløringsproblemer i scheduleren">
<correction dbconfig-common 			"Retter problem forårsaget af ændring i bash' POSIX-virkemåde">
<correction debian-edu-config 			"Anveder PXE-valgmuligheden <q>ipappend 2</q> til boot af LTSP-klient; retter opsætning af sudo-ldap configuration; retter tab af dynamisk allokerede v4 IP-adresser; flere rettelser og forbedringer til/af debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc 			"Opdaterer Debian Edu Buster- og ITIL-vejledninger og oversættelser">
<correction dehydrated 				"Retter hentning af kontooplysninger; opfølgende rettelser af konto-id-håndtering og APIv1-kompatibilitet">
<correction devscripts 				"debchange: opsætter buster-backports med --bpo-valgmuligheden som mål">
<correction dma 				"Begræns ikke TLS-forbindelser til at benytte TLS 1.0">
<correction dpdk 				"Ny stabil opstrømsudgave">
<correction dput-ng 				"Tilføjer kodenavnene buster-backports og stretch-backports-sloppy">
<correction e2fsprogs 				"Retter nedbrud i e4defrag på 32 bit-arkitekturer">
<correction enigmail 				"Ny opstrømsudgave; sikkerhedsrettelser [CVE-2019-12269]">
<correction epiphany-browser 			"Sikrer at webudvidelsen benytte den medfølgende kopi af libdazzle">
<correction erlang-p1-pkix 			"Retter håndtering af GnuTLS-certifikater">
<correction facter 				"Retter fortolkning af Linux-routers ikke-key/value-flag (fx onlink)">
<correction fdroidserver 			"Ny opstrømsudgave">
<correction fig2dev 				"Segfault ikke ved cirkel-/halvcirkel-pilehoveder med en forstørrelse på mere end 42 [CVE-2019-14275]">
<correction firmware-nonfree 			"atheros: Tilføjer Qualcomm Atheros QCA9377 rev 1.0-firmwareversion WLAN.TF.2.1-00021-QCARMSWP-1; realtek: Tilføjer Realtek RTL8822CU Bluetooth-firmware; atheros: Ruller ændring af QCA9377 rev 1.0-firmware i 20180518-1 tilbage; misc-nonfree: Tilføjer firmware til MediaTek MT76x0/MT76x2u-wirelesschips, MediaTek MT7622/MT7668-bluetoothchips, GV100-signeret firmware">
<correction freeorion 				"Retter nedbrud ved indlæsning eller gemning af spildata">
<correction fuse-emulator 			"Foretrækker X11-backend fremfor Waylands; viser Fuse-ikonen i GTK-vinduet og i About-dialogen">
<correction fusiondirectory 			"Strengere kontroller ved LDAP-opslag; tilføjer manglende afhængighed af php-xml">
<correction gcab 				"Retter korruption ved udpakning">
<correction gdb 				"Genopbygger mod ny libbabeltrace">
<correction glib2.0 				"Får GKeyFile-opsætningsbackend til at oprette ~/.config og opsætningsfiler med begrænsede rettigheder [CVE-2019-13012]">
<correction gnome-bluetooth 			"Undgår nedbrud i GNOME Shell når gnome-shell-extension-bluetooth-quick-connect anvendes">
<correction gnome-control-center 		"Retter nedbrud når panelet Details -&gt; Overview (info-overview) er valgt; retter hukommelseslækager i Universal Access-panelet; retter en regression som forårsagede at valgmulighederne Universal Access -&gt; Zoom mouse tracking ikke fungerede; opdaterer islandske og japanske oversættelser">
<correction gnupg2 				"Tilbagefører mange fejl- og stabilitetsrettelser fra opstrøm; anvender keys.openpgp.org som standardkeyserver; importerer som standard kun selvsignerede signaturer">
<correction gnuplot 				"Retter ufuldstændig/usikker initialisering af ARGV-array">
<correction gosa 				"Strengere kontroller ved LDAP-opslag">
<correction hfst 				"Sikrer mere gnidningsløse opgraderinger fra stretch">
<correction initramfs-tools 			"Deaktiverer resume når der ikke er en brugbar swap-enhed; MODULES=most: Medtager alle tastaturdrivermoduler, cros_ec_spi- og SPI-drivere, extcon-usbc-cros-ec; MODULES=dep: Medtager extcon-drivere">
<correction jython 				"Bevarer bagudkompabilitet med Java 7">
<correction lacme 				"Opdaterer med fjernelse af uautentificeret GET-understøttelse fra Let's Encrypts ACMEv2-API">
<correction libblockdev 			"Anvender eksisterende cryptsetup-API til ændring af keyslot-passphrase">
<correction libdatetime-timezone-perl 		"Opdaterer medfølgende data">
<correction libjavascript-beautifier-perl 	"Tilføjer understøttelse af operatoren <q>=&gt;</q>">
<correction libsdl2-image 			"Retter bufferoverløb [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635]; retter tilgang udenfor grænserne i PCX-håndtering [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img 				"Holder op med at anvende interne kopier af JPEG-, Zlib- og PixarLog-codecs, retter nedbrud">
<correction libxslt 				"Retter omgåelse af sikkerhedsframework [CVE-2019-11068], uinitialiseret læsning af xsl:number-token [CVE-2019-13117] og uinitialiseret læsning med UTF-8-grupperingstegn [CVE-2019-13118]">
<correction linux 				"Ny stabil opstrømsudgave">
<correction linux-latest 			"Opdaterer til 4.19.0-6-kernel-ABI">
<correction linux-signed-amd64 			"Ny stabil opstrømsudgave">
<correction linux-signed-arm64 			"Ny stabil opstrømsudgave">
<correction linux-signed-i386 			"Ny stabil opstrømsudgave">
<correction lttv 				"Genopbygger mod ny libbabeltrace">
<correction mapproxy 				"Retter WMS Capabilities med Python 3.7">
<correction mariadb-10.3 			"Ny stabil opstrømsudgave; sikkerhedsrettelser [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805]; retter segfault ved 'information_schema'-tilgang; opdøber 'mariadbcheck' til 'mariadb-check'">
<correction musescore 				"Deaktiverer webkit-funktionalitet">
<correction ncbi-tools6 			"Genpakker uden ikke-fri data/UniVec.*">
<correction ncurses 				"Fjerner <q>rep</q> fra xterm-new og afledte terminfo-beskrivelser">
<correction netdata 				"Fjerner Google Analytics fra genereret dokumentation; fravælg at sende anonym statistik; fjerner <q>log på</q>-knap">
<correction newsboat 				"Retter problem med anvendelse efter frigivelse">
<correction nextcloud-desktop 			"Tilføjer manglende afhængighed af nextcloud-desktop-common til nextcloud-desktop-cmd">
<correction node-lodash 			"Retter prototypeforurening [CVE-2019-10744]">
<correction node-mixin-deep 			"Retter problem med prototypeforurening">
<correction nss 				"Retter sikkerhedsproblemer [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs 				"Retter et antal hukommelseslækager">
<correction open-infrastructure-compute-tools 	"Retter containerstart">
<correction open-vm-tools 			"Håndterer på korrekt vis styresystemsversioner på formen <q>X</q>, frem for <q>X.Y</q>">
<correction openldap 				"Begrænser rootDN-proxyauthz til dens egne databaser [CVE-2019-13057]; håndhæver sasl_ssf's ACL-statement ved hver forbindelse [CVE-2019-13565]; retter slapo-rwm til ikke at frigive oprindeligt filter når det genskrevne filter er ugyldigt">
<correction osinfo-db 				"Tilføjer oplysningerer om buster 10.0; retter URL'er til stretch-download; retter navnet på parameteret der anvendes til at opsætte fullname når der genereres en preseed-fil">
<correction osmpbf 				"Genopbygget med protobuf 3.6.1">
<correction pam-u2f 				"Retter usikker håndtering af debugfil [CVE-2019-12209]; retter lækage af debugfildescriptor [CVE-2019-12210]; retter tilgang udenfor grænserne; retter segfault efter mislykket forsøg på at allokere en buffer">
<correction passwordsafe 			"Installerer lokaltilpasningsfiler i den korrekte mappe">
<correction piuparts 				"Opdaterer opsætninger til udgaven buster; retter falske mislykkede forsøg på at fjerne pakker med navne der slutter med <q>+</q>; genererer separate tarball-navne til <q>--merged-usr</q>-chroots">
<correction postgresql-common 			"Retter pg_upgradecluster fra postgresql-common 200, 200+deb10u1, 201 og 202 ødelægger data_directory-indstillingen når den anvendes *to gange* til at opgradere en klynge (fx 9.6 -&gt; 10 -&gt; 11)">
<correction pulseaudio 				"Retter genetablering af mute-tilstand">
<correction puppet-module-cinder 		"Retter forsøg på at skrive til /etc/init">
<correction python-autobahn 			"Retter pyqrcode-opbygningsafhængigheder">
<correction python-django 			"Ny sikkerhedsudgivelse fra opstrøm [CVE-2019-12781]">
<correction raspi3-firmware 			"Tilføjer understøttelse af Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite og Raspberry Pi Compute Module IO Board V3">
<correction reportbug 				"Opdaterer udgivelsesnavne, efter udgivelse af buster; genaktiverer <q>stretch-pu</q>-forespørgsler; retter nedbrud i pakke-/version-opslag; tilføjer manglende afhængighed af sensible-utils">
<correction ruby-airbrussh 			"Smid ikke en exception ved ugyldigt UTF-8 SSH-output">
<correction sdl-image1.2 			"Retter bufferoverløb [CVE-2019-5052 CVE-2019-7635], tilgang udenfor grænserne [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail 				"sendmail-bin.postinst, initscript: Lad start-stop-daemon matche på pidfile og udførbar fil; sendmail-bin.prerm: Stopper sendmail før alternativerne fjernes">
<correction slirp4netns 			"Ny stabil opstrømsudgave med sikkerhedsrettelser - tjek resultat fra sscanf når der emuleres ident [CVE-2019-9824]; retter heapoverløb i medfølgende libslirp [CVE-2019-14378]">
<correction systemd 				"Network: Retter mislykket forsøg på at aktivere interface med Linux kernel 5.2; ask-password: Forhindrer bufferoverløb når der læses fra keyring; network: Opfører sig pænere når IPv6 er deaktiveret">
<correction tzdata 				"Ny opstrømsudgave">
<correction unzip 				"Retter problemer med zip-bombe [CVE-2019-13232]">
<correction usb.ids 				"Rutinemæssig opdatering af USB-id'er">
<correction warzone2100 			"Retter en segmenteringsfejl når man er vært for et spil med flere spillere">
<correction webkit2gtk 				"Ny stabil opstrømsversion; kræver ikke længere CPU'er med SSE2">
<correction win32-loader 			"Genopbygger mod aktuelle pakker, i særdeleshed debian-archive-keyring; retter opbygningsfejl ved at håndhæve en POSIX-locale">
<correction xymon 				"Retter flere sikkerhedsproblemer (kun server) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization 		"Tilbagefører yderligere sikkerhedsforanstaltninger">
<correction z3 					"Opsæt ikke SONAME hørende til libz3java.so til libz3.so.4">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction pump 				"Vedligeholdes ikke; sikkerhedsproblemer">
<correction rustc 				"Fjerner forældet rust-doc">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
