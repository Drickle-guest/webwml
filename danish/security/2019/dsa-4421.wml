#use wml::debian::translation-check translation="5357ead7bb298e3857977fea7b0087543c6072b3" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i webbrowseren chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

    <p>Zhe Jin opdagede et problem med anvendelse efter frigivelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

    <p>Mark Brand opdagede et problem med anvendelse efter frigivelse i
    implementeringen af FileAPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

    <p>Mark Brand opdagede et problem med anvendelse efter frigivelse i
    implementeringen af WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

    <p>Dimitri Fourny opdagede et bufferoverløbsproblem i JavaScript-biblioteket 
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

    <p>Choongwoo Han opdagede et typeforvirringsproblem i JavaScript-biblioteket 
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

    <p>pdknsk opdagede et heltalsoverløbsproblem i biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

    <p>Jun Kokatsu opdagede et rettighedsproblewm i implementeringen af 
    Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

    <p>Juno Im of Theori opdagede et problem med forfalskning af 
    brugergrænsefladen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

    <p>pdknsk opdagede et heltalsoverløbsproblem i biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

    <p>Mark Brand opdagede en kapløbstilstand i implementeringen af 
    Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

    <p>Mark Brand opdagede en kapløbstilstand i implementeringen af 
    DOMStorage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

    <p>Tran Tien Hung opdagede et problem med læsning udenfor grænserne i 
    biblioteket skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

    <p>sohalt opdagede en måde at omgå Content Security Policy på.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

    <p>Jun Kokatsu opdagede en måde at omgå Content Security Policy på.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

    <p>Ronni Skansing opdagede et problem med forfalskning af 
    brugergrænsefladen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

    <p>Andrew Comminos opdagede en måde at omgå Content Security Policy 
    på.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 73.0.3683.75-1~deb9u1.</p>

<p>Vi anbefaler at du opgraderer dine chromium-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende chromium, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
