<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>When using an OCSP responder Tomcat Native did not correctly handle
invalid responses. This allowed for revoked client certificates to be
incorrectly identified. It was therefore possible for users to
authenticate with revoked certificates when using mutual TLS. Users
not using OCSP checks are not affected by this vulnerability.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.1.32~repack-2+deb8u2.</p>

<p>We recommend that you upgrade your tomcat-native packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1475.data"
# $Id: $
