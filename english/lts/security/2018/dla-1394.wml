<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities were discovered in ImageMagick, an
image manipulation program, that allow remote attackers to cause denial
of service (application crash) or out of bounds memory access via
crafted SUN, BMP, or DIB image files.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8:6.8.9.9-5+deb8u13.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1394.data"
# $Id: $
