#use wml::debian::translation-check translation="8bf863e73b414ad1568d2047c5d14e060c5ce5fd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans libvirt, une API d’abstraction
pour divers mécanismes de virtualisation.</>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10161">CVE-2019-10161</a>

<p>Des clients, avec les droits de lecture seuls, pourraient utiliser l’API
pour indiquer un chemin arbitraire qui pourrait être accédé avec les permissions
du processus libvirtd. Un attaquant ayant accès à la socket de libvirtd pourrait
utiliser cela pour enquêter sur l’existence de fichiers arbitraires, causer un
déni de service ou autrement faire que libvirtd exécute des programmes
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10167">CVE-2019-10167</a>

<p>Une vulnérabilité d’exécution de code arbitraire à l’aide de l’API où un
binaire précisé par l’utilisateur est utilisé pour enquêter sur les capacités de
domaine. Des clients, avec les droits de lecture seuls, pourraient indiquer un
chemin arbitraire pour cet argument, faisant que libvirtd exécute un exécutable
contrefait avec ses propres privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.9-9+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libvirt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1832.data"
# $Id: $
