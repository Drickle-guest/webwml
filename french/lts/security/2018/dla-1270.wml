#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen, qui
pouvaient avoir pour conséquence une augmentation de droits.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.1.6.lts1-12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Veuillez noter que <a href="https://security-tracker.debian.org/tracker/CVE-2017-15590">CVE-2017-15590</a>
(XSA-237) ne sera <em>pas</em> corrigé dans Wheezy parce que les correctifs
sont trop intrusifs pour être rétroportés. La vulnérabilité peut être 
atténuée en ne passant pas par des périphériques physiques pour atteindre
des hôtes non fiables. Plus d'informations peuvent être trouvées sur
<a href="https://xenbits.xen.org/xsa/advisory-237.html">https://xenbits.xen.org/xsa/advisory-237.html</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1270.data"
# $Id: $
